name := "ladep"

version := "0.1"

scalaVersion := "2.12.10"

lazy val macros = (project in file("macros")).settings(
    libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value
)

lazy val core = (project in file("core")) dependsOn macros
