package com.github.louisjl.ladep

object App {
    private val input: Array[Byte] =
        System.in.readAllBytes()

    type TypeAddressAlias = Byte
    type TypeAddress <: TypeAddressAlias
    object TypeAddress {
        @inline def apply(typeAddress: Int): TypeAddress =
            TypeAddress(typeAddress.toByte)

        @inline def read(input: Iterator[Byte]): TypeAddress =
            TypeAddress(input.next())

        @inline def apply(typeAddress: TypeAddressAlias): TypeAddress =
            typeAddress.asInstanceOf[TypeAddress]
    }

    val SECTION_SEPARATOR: Byte = 0.toByte
    val RESERVED_WORDS: List[Byte] = List(SECTION_SEPARATOR)

    val DEFAULT_TYPES: List[Type] = Nil
    val TYPE_ADDRESS_OFFSET: Int = RESERVED_WORDS.size + DEFAULT_TYPES.size

    type TypeAlias = Byte
    type Type <: TypeAlias
    object Type {
        @inline def empty: Type =
            Nil.asInstanceOf[Type]

        @inline def apply(size: Byte): Type =
            size.asInstanceOf[Type]

        @inline def read(inputIterator: Iterator[Type]): Type =
            Type(inputIterator.next())

        @inline def get(address: TypeAddress, types: List[Type]): Type =
            types((types.size - 1 - DEFAULT_TYPES.size) - (address - TYPE_ADDRESS_OFFSET))

        @inline def add(dataType: Type, types: List[Type]): List[Type] =
            dataType :: types
    }

    type DataAlias = List[Byte]
    type Data <: DataAlias
    object Data {
        @inline def empty: Data =
            Nil.asInstanceOf[Data]

        @inline def apply(data: DataAlias): Data =
            data.asInstanceOf[Data]

        @inline def add(byte: Byte, data: List[Byte]): Data =
            Data(byte :: data)
    }

    type TypedDataAlias = (TypeAddress, Data)
    type TypedData <: TypedDataAlias
    object TypedData {
        @inline def apply(typeAddress: TypeAddressAlias, data: DataAlias): TypedData =
            (typeAddress, data).asInstanceOf[TypedData]

        @inline def read(dataTypeAddress: TypeAddress, types: List[Type], input: Iterator[Byte]): TypedData = {
            var currentData = Data.empty
            val currentType = Type.get(dataTypeAddress, types)
            for(_ <- 0 until currentType) {
                currentData = Data.add(input.next, currentData)
            }
            TypedData(dataTypeAddress, currentData)
        }
    }

    def main(args: Array[String]): Unit = {
        val (types, remainder) = parseTypes(this.input)
        val data = parseTypedData(types, remainder)

        println(types.mkString(", "))
        println(data.mkString(", "))
    }

    private def parseTypes(input: Array[Byte]): (List[Type], Array[Byte]) = {
        var types: List[Type] = Nil

        val inputIterator = input.iterator

        for (byte <- inputIterator) {
            if (byte != SECTION_SEPARATOR) {
                types = Type.add(Type(byte), types)
            }
            else return (types, inputIterator.toArray)
        }
        (types, Array.empty)
    }

    def parseTypedData(types: List[Type], input: Array[Byte]): List[TypedData] = {
        var typedData: List[TypedData] = Nil

        val inputIterator = input.iterator

        for (byte <- inputIterator) {
            val currentTypeAddress = TypeAddress(byte)
            typedData :+= TypedData.read(currentTypeAddress, types, inputIterator)
        }

        typedData
    }
}
