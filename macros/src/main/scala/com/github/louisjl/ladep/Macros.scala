package com.github.louisjl.ladep

import scala.language.experimental.macros
import scala.reflect.macros.blackbox

object Macros {
    final def memberName: String = macro MacrosImpl.memberNameImpl
}

object MacrosImpl {
    final def memberImpl(c: blackbox.Context)(struct: c.Expr[Struct]): c.Expr[Unit] = {
        import c.universe._
        c.Expr(q"()")
    }

    final def memberNameImpl(c: blackbox.Context): c.Expr[String] = {
        import c.universe._
        val owner = c.internal.enclosingOwner
        val name = owner.fullName

        c.Expr[String](q"$name")
    }
}
