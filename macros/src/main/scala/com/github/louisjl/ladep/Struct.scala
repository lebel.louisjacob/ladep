package com.github.louisjl.ladep

import scala.language.experimental.macros

abstract class Struct {
    final def member(struct: Struct): Unit = macro MacrosImpl.memberImpl
}
